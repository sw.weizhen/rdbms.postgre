package main

import (
	"fmt"

	pg "gitlab.com/sw.weizhen/rdbms.postgre"
)

func init() {
	db, err = pg.New(pg.DBOpts{
		Host:         "localhost",
		User:         "superset",
		Password:     "superset",
		Database:     "superset",
		Port:         5432,
		MaxIdleConns: 1,
		MaxOpenConns: 5,
	})
}

var db *pg.PostgreDB
var err error

func main() {
	if err != nil {
		fmt.Printf("init postgre fail: %v\n", err)
		return
	}

	sqlSel := "select au.username, au.first_name, au.last_name, au.active, au.email, au.created_on from superset.public.ab_user au where au.username = $1 or au.username = $2"
	resp, err := db.Query(sqlSel, "sw", "gillain")
	if err != nil {
		fmt.Printf("query fail: %v\n", err)
		return
	}

	fmt.Printf("response len: %v\n", resp.Len)

	for _, v := range resp.RespMap {
		fmt.Println(v)
	}

}
