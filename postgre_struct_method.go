package swpostgre

import (
	"fmt"
)

func (ref *PostgreDB) Status() bool {
	if ref.conn == nil {
		return false
	}

	return ref.conn.Ping() == nil
}

func (ref *PostgreDB) Close() {
	if ref.conn != nil {
		ref.conn.Close()
	}
}

func (ref *PostgreDB) SetMaxIdleConns(maxConns int) {
	ref.conn.SetMaxIdleConns(maxConns)
}

func (ref *PostgreDB) SetMaxOpenConns(maxConns int) {
	ref.conn.SetMaxOpenConns(maxConns)
}

func (ref *PostgreDB) Query(sqlCmd string, args ...interface{}) (*pgResp, error) {
	if ref.conn == nil {
		return nil, fmt.Errorf("nil conn ptr, dsn: %s", ref.dsn)
	}

	rows, err := ref.conn.Query(sqlCmd, args...)
	if err != nil {
		return nil, err
	}

	rowsMP, err := rowsToMap(rows)
	if err != nil {
		return nil, err
	}

	return &pgResp{
		RespMap: rowsMP,
		Len:     uint32(len(rowsMP)),
	}, nil
}

func (ref *PostgreDB) Exec(sqlCmd string, args ...interface{}) (int64, error) {
	if ref.conn == nil {
		return 0, fmt.Errorf("nil conn ptr, dsn: %s", ref.dsn)
	}

	result, err := ref.conn.Exec(sqlCmd, args...)
	if err != nil {
		return 0, err
	}

	return result.RowsAffected()
}
